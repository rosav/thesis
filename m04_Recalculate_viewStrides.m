%% Recalculate_viewStrides_ST
% as it is known that CV is influences by amount of strides, we evalute how
% our stats would have changes if we would have included a specific amount
% of strides.

% R. Visscher, May 2022

clc
%% Set-up
Group = 'TD';%'Orthosis';%CP, TD, Orthosis
n_steps=12;
n_runs=100;
SpatioTemporal_MDC = SpatioTemporal_CPTD;
list = fieldnames(SpatioTemporal_MDC.(Group));
count=1;

%% Spatio-Temporal get data
for i = 1:length(list)
    %% Extract data ST
    % Calculate Mean, SD, CV
    if SpatioTemporal_MDC.(Group).(list{i,1}).AmountStrides_ST>24
    Strides_ST_L = length(SpatioTemporal_MDC.(Group).(list{i,1}).Left.Stride_Time);
    Strides_ST_R = length(SpatioTemporal_MDC.(Group).(list{i,1}).Right.Stride_Time);
    
    for k=1:n_runs
        r_L= randi([1 Strides_ST_L],1,n_steps);
        r_R = randi([1 Strides_ST_R],1,n_steps);
        
        for j=1:length(r_L)
            n_L = r_L(j);
            ST_L(j) = SpatioTemporal_MDC.(Group).(list{i,1}).Left.Stride_Time(n_L);
            n_R = r_R(j);
            ST_R(j) = SpatioTemporal_MDC.(Group).(list{i,1}).Right.Stride_Time(n_R);
        end
        
        %nanmean
        SpatioTemporal_steps.(Group).(list{i,1}).Mean_Left.Stride_Time(k) = nanmean(ST_L);
        SpatioTemporal_steps.(Group).(list{i,1}).Mean_Right.Stride_Time(k) = nanmean(ST_R);
        %nansd
        SpatioTemporal_steps.(Group).(list{i,1}).SD_Left.Stride_Time(k) = nanstd(ST_L);
        SpatioTemporal_steps.(Group).(list{i,1}).SD_Right.Stride_Time(k) = nanstd(ST_R);
        %CV
        SpatioTemporal_steps.(Group).(list{i,1}).CV_Left.Stride_Time(k) = SpatioTemporal_steps.(Group).(list{i,1}).SD_Left.Stride_Time(k)/SpatioTemporal_steps.(Group).(list{i,1}).Mean_Left.Stride_Time(k)*100;
        SpatioTemporal_steps.(Group).(list{i,1}).CV_Right.Stride_Time(k) = SpatioTemporal_steps.(Group).(list{i,1}).SD_Right.Stride_Time(k)/SpatioTemporal_steps.(Group).(list{i,1}).Mean_Right.Stride_Time(k)*100;
        SpatioTemporal_steps.(Group).(list{i,1}).CV_All.Stride_Time(k) = (mean(SpatioTemporal_steps.(Group).(list{i,1}).CV_Left.Stride_Time(k))+mean(SpatioTemporal_steps.(Group).(list{i,1}).CV_Right.Stride_Time(k)))/2;
    
    end%for n_runs
    count=count+1;
    end%if amount strides
end %end for-loop subjects

%% Order data - CP/TD
% Order to large lists 

Subs = fieldnames(SpatioTemporal_steps.(Group));
for n_subs=1:length(Subs)
     
     T_TD_steps.twelf_strides(n_subs,1:n_runs) = SpatioTemporal_steps.(Group).(Subs{n_subs,1}).CV_All.Stride_Time;
    
end %end for-loop subjects
clear var Strides_ST_L Strides_ST_R r_L r_R ST_L ST_R SpatioTemporal_steps

%% Plot data
Overview_re.(Group).two = mean(T_TD_steps.two_strides);
Overview_re.(Group).four = mean(T_TD_steps.four_strides);
Overview_re.(Group).six = mean(T_TD_steps.six_strides);
Overview_re.(Group).eight = mean(T_TD_steps.eight_strides);
Overview_re.(Group).ten = mean(T_TD_steps.ten_strides);
Overview_re.(Group).twelf = mean(T_TD_steps.twelf_strides);
Overview_re.(Group).all = mean(T_TDCP_recal.(Group).Stride_Time);

figure
hold on
histogram(Overview_re.(Group).two)
histogram(Overview_re.(Group).four)
histogram(Overview_re.(Group).six)
histogram(Overview_re.(Group).eight)
histogram(Overview_re.(Group).ten)
histogram(Overview_re.(Group).twelf)
plot(Overview_re.(Group).all,25,'*r')

