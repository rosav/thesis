function [yMean,ySD,CI_y] = f_CI95(y)
%f_CI95 calculates 95% confidence interval of normally distributed data
%   input: y, column vector
%   output: mean, SD, 95% CI of vector y 

% R Visscher, 03/2022

N = size(y,1);                                      % Number of �Experiments� In Data Set
yMean = mean(y);                                    % Mean Of All Experiments At Each Value Of �x�
ySD = std(y);
ySEM = std(y)/sqrt(N);                              % Compute �Standard Error Of The Mean� Of All Experiments At Each Value Of �x�
CI95 = tinv([0.025 0.975], N-1);                    % Calculate 95% Probability Intervals Of t-Distribution
yCI95 = bsxfun(@times, ySEM, CI95(:));
CI_y = yCI95+yMean;
end

