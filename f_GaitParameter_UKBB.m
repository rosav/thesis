%{ 
*********************************************************************************
     Function "f_calc_GaitParameter" linked to xxx
                          runs from "xxx"
                    by Frank Behrendt April 2018
                   Adapted by R Visscher Oct 2021
*********************************************************************************
https://www.vicon.com/faqs/software/how-does-nexus-plug-in-gait-and-polygon-calculate-gait-cycle-parameters-spatial-and-temporal
- Walking speed: is stride length divided by stride time
- Stride time: time between successive ipsilateral foot strikes
- Stride length: is the distance from IP1 to IP2
- Step length is the distance from contralateral Foot off to ispilateral Foot Strike
- Single support: time from contralateral foot off to contralateral foot contact
- Opposite Foot Contact: time of contralateral foot contact
- Foot Off: time of ipsilateral foot off
- DoubleSupport: Duration from Foot Strike contra to Foot Off ipsi
- Cadence: number of strides per unit time (usually per minute)
- Stride width: is the distance from CP to CPP 
    IP1 is the ipsilateral marker's position at the first ipsilateral foot contact.
    IP2 is the ipsilateral marker's position at the second ipsilateral foot contact.
    CP is the contralateral marker's position at the contralateral foot contact.
    CPP is CP projected onto the IP1 to IP2 vector.

INPUT
    MarkersC3D = Struct with all markers similar to output from c3d but with corrected gait direction
    freq = Vicon frequency

OUTPUT

%}


function GaitParam = f_GaitParameter_UKBB(MarkersC3D,EventsFrames,freq)

%% Stride length and time
%Right
for i=1:(length(EventsFrames.Right_Foot_Strike)-1) 
       LengthGaitcycle(i) = EventsFrames.Right_Foot_Strike(i+1)-EventsFrames.Right_Foot_Strike(i); %in frames
       stride(i,1:2) = MarkersC3D.RTOE(EventsFrames.Right_Foot_Strike(i+1),1:2) - MarkersC3D.RTOE(EventsFrames.Right_Foot_Strike(i),1:2);
       
       GaitParam.Dummy.Stride_Length.Right(i) = sqrt((stride(i,1))^2 + (stride(i,2))^2)/1000;  % Stride length in m
       GaitParam.Dummy.Stride_Time.Right(i) = (EventsFrames.Right_Foot_Strike(i+1)-EventsFrames.Right_Foot_Strike(i))/freq;  % Stride time in seconds
       GaitParam.Dummy.Walking_Speed.Right(i) = GaitParam.Dummy.Stride_Length.Right(i) / GaitParam.Dummy.Stride_Time.Right(i); % Walking Speed in m/s
       GaitParam.Dummy.Cadence.Right(i) = (60 / GaitParam.Dummy.Stride_Time.Right(i))*2; % Cadence in step per minute
       
       % SingleSupport in s
       if i>length(EventsFrames.Left_Foot_Off)
           continue
       elseif i>length(EventsFrames.Left_Foot_Strike)
           continue
       else
           if EventsFrames.Left_Foot_Strike(i)< EventsFrames.Left_Foot_Off(i)
               if (i+1)>length(EventsFrames.Left_Foot_Strike)
                   continue
               else
                   GaitParam.Dummy.Single_Support.Right(i) = (EventsFrames.Left_Foot_Strike(i+1)-EventsFrames.Left_Foot_Off(i))/freq;% SingleSupport in s
               end
           else
               GaitParam.Dummy.Single_Support.Right(i) = (EventsFrames.Left_Foot_Strike(i)-EventsFrames.Left_Foot_Off(i))/freq;
           end
       end
      
       % DoubleSupport in s
       % Double support: time from ipsilateral foot contact to contralateral foot off plus time from contralateral foot contact to ipsilateral foot off. 
       if i>length(EventsFrames.Left_Foot_Off)
           continue
       elseif i>length(EventsFrames.Right_Foot_Off)
           continue
       else
           if EventsFrames.Right_Foot_Strike(i)<EventsFrames.Left_Foot_Off(i) && EventsFrames.Left_Foot_Strike(i)< EventsFrames.Right_Foot_Off(i)
               GaitParam.Dummy.Double_Support.Right(i) = (EventsFrames.Left_Foot_Off(i)-EventsFrames.Right_Foot_Strike(i)+EventsFrames.Right_Foot_Off(i)-EventsFrames.Left_Foot_Strike(i))/freq;
           elseif EventsFrames.Right_Foot_Strike(i)>EventsFrames.Left_Foot_Off(i) && EventsFrames.Left_Foot_Strike(i)< EventsFrames.Right_Foot_Off(i)
               if (i+1)>length(EventsFrames.Left_Foot_Off)
                   continue
               else
                   GaitParam.Dummy.Double_Support.Right(i) = (EventsFrames.Left_Foot_Off(i+1)-EventsFrames.Right_Foot_Strike(i)+EventsFrames.Right_Foot_Off(i)-EventsFrames.Left_Foot_Strike(i))/freq;
               end
           elseif EventsFrames.Right_Foot_Strike(i)>EventsFrames.Left_Foot_Off(i) && EventsFrames.Left_Foot_Strike(i)> EventsFrames.Right_Foot_Off(i)
               if (i+1)>length(EventsFrames.Right_Foot_Off)
                   continue
               elseif (i+1)>length(EventsFrames.Left_Foot_Off)
                   GaitParam.Dummy.Double_Support.Right(i) = (EventsFrames.Left_Foot_Off(i+1)-EventsFrames.Right_Foot_Strike(i)+EventsFrames.Right_Foot_Off(i+1)-EventsFrames.Left_Foot_Strike(i))/freq;
               end
           elseif EventsFrames.Right_Foot_Strike(i)<EventsFrames.Left_Foot_Off(i) && EventsFrames.Left_Foot_Strike(i)> EventsFrames.Right_Foot_Off(i)
               if (i+1)>length(EventsFrames.Right_Foot_Off)
                   continue
               else
                   GaitParam.Dummy.Double_Support.Right(i) = (EventsFrames.Left_Foot_Off(i)-EventsFrames.Right_Foot_Strike(i)+EventsFrames.Right_Foot_Off(i+1)-EventsFrames.Left_Foot_Strike(i))/freq;
               end
           end
       end
       %% Stride width in metres between ankle joint centres
       % Stride width: is the distance from CP to CPP 
    % IP1 is the ipsilateral marker's position at the first ipsilateral foot contact.
    % IP2 is the ipsilateral marker's position at the second ipsilateral foot contact.
    % CP is the contralateral marker's position at the contralateral foot contact.
    % CPP is CP projected onto the IP1 to IP2 vector.
    
    % R Visscher adapted LTIO/RTIO to LTMT/RTMT  
    % Reference: https://www.vicon.com/faqs/software/how-does-nexus-plug-in-gait-and-polygon-calculate-gait-cycle-parameters-spatial-and-temporal
       IP1 = MarkersC3D.RTIO(EventsFrames.Right_Foot_Strike(i),1:2);
       IP2 = MarkersC3D.RTIO(EventsFrames.Right_Foot_Strike(i+1),1:2);
       CP  = MarkersC3D.LTIO(EventsFrames.Left_Foot_Strike(i),1:2); 
       Sign_side = -1;
       
       Vec_IP = IP1-IP2; % vector IP2 to IP1
       Norm_Vec_IP = Vec_IP./norm(Vec_IP);
       Vec_IP2_CP = CP-IP2; % vector IP2 to CP
       Vec3 = Norm_Vec_IP * dot(Vec_IP2_CP,Norm_Vec_IP); % Vector from IP2 to insertion of orthogonal vector to IP1-IP2 and CP
       Dist = norm (Vec_IP2_CP-Vec3); % Distance between vectro IP1-IP2 to CP

       StrideWidth = Dist * Sign_side; %depending on body side

       % if the feet cross, calculate negative step width
       Vec_IP_3 = [Vec_IP,0];
       Vec_IP2_CP_3 = [Vec_IP2_CP,0];
       CrossProd = cross(Vec_IP_3,Vec_IP2_CP_3);
       if CrossProd(:,3) < 0 
          StrideWidth = StrideWidth * -1;
       end %IF cross(Vec_IP,Vec_IP2_CP) < 0 

       GaitParam.Dummy.Stride_Width.Right(i) = StrideWidth/1000; % transfer from millimeters to metres     
       
end %for-loop right events

clear i stride LengthGaitcycle

%Left 
for i=1:(length(EventsFrames.Left_Foot_Strike)-1) 
       LengthGaitcycle(i) = EventsFrames.Left_Foot_Strike(i+1)-EventsFrames.Left_Foot_Strike(i); %in frames
       stride(i,1:2) = MarkersC3D.LTOE(EventsFrames.Left_Foot_Strike(i+1),1:2) - MarkersC3D.LTOE(EventsFrames.Left_Foot_Strike(i),1:2);
       
       %% Stride length (m) and time (s)
       GaitParam.Dummy.Stride_Length.Left(i) = sqrt((stride(i,1))^2 + (stride(i,2))^2)/1000;  % Stride length in m
       GaitParam.Dummy.Stride_Time.Left(i)=(EventsFrames.Left_Foot_Strike(i+1)-EventsFrames.Left_Foot_Strike(i))/freq; % Stride time in seconds
       
       %% Walking speed and cadance
       GaitParam.Dummy.Walking_Speed.Left(i) = GaitParam.Dummy.Stride_Length.Left(i) / GaitParam.Dummy.Stride_Time.Left(i); % Walking Speed in m/s 
       GaitParam.Dummy.Cadence.Left(i) = (60 / GaitParam.Dummy.Stride_Time.Left(i))*2;% Cadence in step per minute
       
       %% SingleSupport in s
       if i>length(EventsFrames.Right_Foot_Off)
           continue
       elseif i>length(EventsFrames.Right_Foot_Strike)
           continue
       else
           if EventsFrames.Right_Foot_Strike(i)< EventsFrames.Right_Foot_Off(i)
               if (i+1)>length(EventsFrames.Right_Foot_Strike)
                   continue
               else
                   GaitParam.Dummy.Single_Support.Left(i) = (EventsFrames.Right_Foot_Strike(i+1)-EventsFrames.Right_Foot_Off(i))/freq;
               end
           else
               GaitParam.Dummy.Single_Support.Left(i) = (EventsFrames.Right_Foot_Strike(i)-EventsFrames.Right_Foot_Off(i))/freq;
           end
       end
       
       %% DoubleSupport in s
       % Double support: time from ipsilateral foot contact to contralateral foot off plus time from contralateral foot contact to ipsilateral foot off. 
       if i>length(EventsFrames.Left_Foot_Off)
           continue
       elseif i>length(EventsFrames.Right_Foot_Off)
           continue
       else 
           if EventsFrames.Left_Foot_Strike(i)<EventsFrames.Right_Foot_Off(i) && EventsFrames.Right_Foot_Strike(i)< EventsFrames.Left_Foot_Off(i)
               GaitParam.Dummy.Double_Support.Left(i) = (EventsFrames.Right_Foot_Off(i)-EventsFrames.Left_Foot_Strike(i)+EventsFrames.Left_Foot_Off(i)-EventsFrames.Right_Foot_Strike(i))/freq;
           elseif EventsFrames.Left_Foot_Strike(i)>EventsFrames.Right_Foot_Off(i) && EventsFrames.Right_Foot_Strike(i)< EventsFrames.Left_Foot_Off(i)
               if (i+1)>length(EventsFrames.Right_Foot_Off)
                   continue
               else
                   GaitParam.Dummy.Double_Support.Left(i) = (EventsFrames.Right_Foot_Off(i+1)-EventsFrames.Left_Foot_Strike(i)+EventsFrames.Left_Foot_Off(i)-EventsFrames.Right_Foot_Strike(i))/freq;
               end
           elseif EventsFrames.Left_Foot_Strike(i)>EventsFrames.Right_Foot_Off(i) && EventsFrames.Right_Foot_Strike(i)> EventsFrames.Left_Foot_Off(i)
               if (i+1)>length(EventsFrames.Right_Foot_Off)
                   continue
               elseif (i+1)>length(EventsFrames.Left_Foot_Off)
                   continue
               else
                   GaitParam.Dummy.Double_Support.Left(i) = (EventsFrames.Right_Foot_Off(i+1)-EventsFrames.Left_Foot_Strike(i)+EventsFrames.Left_Foot_Off(i+1)-EventsFrames.Right_Foot_Strike(i))/freq;
               end
           elseif EventsFrames.Left_Foot_Strike(i)<EventsFrames.Right_Foot_Off(i) && EventsFrames.Right_Foot_Strike(i)> EventsFrames.Left_Foot_Off(i)
               if (i+1)>length(EventsFrames.Left_Foot_Off)
                   continue
               else
                   GaitParam.Dummy.Double_Support.Left(i) = (EventsFrames.Right_Foot_Off(i)-EventsFrames.Left_Foot_Strike(i)+EventsFrames.Left_Foot_Off(i+1)-EventsFrames.Right_Foot_Strike(i))/freq;
               end
           end
       end
       
       %% Stride width in metres between ankle joint centres
       % Stride width: is the distance from CP to CPP 
    % IP1 is the ipsilateral marker's position at the first ipsilateral foot contact.
    % IP2 is the ipsilateral marker's position at the second ipsilateral foot contact.
    % CP is the contralateral marker's position at the contralateral foot contact.
    % CPP is CP projected onto the IP1 to IP2 vector.
    % 
    % Reference: https://www.vicon.com/faqs/software/how-does-nexus-plug-in-gait-and-polygon-calculate-gait-cycle-parameters-spatial-and-temporal
       IP1 = MarkersC3D.LTIO(EventsFrames.Left_Foot_Strike(i),1:2);
       IP2 = MarkersC3D.LTIO(EventsFrames.Left_Foot_Strike(i+1),1:2);
       CP  = MarkersC3D.RTIO(EventsFrames.Right_Foot_Strike(i),1:2); 
       Sign_side = 1;
       
       Vec_IP = IP1-IP2; % vector IP2 to IP1
       Norm_Vec_IP = Vec_IP./norm(Vec_IP);
       Vec_IP2_CP = CP-IP2; % vector IP2 to CP
       Vec3 = Norm_Vec_IP * dot(Vec_IP2_CP,Norm_Vec_IP); % Vector from IP2 to insertion of orthogonal vector to IP1-IP2 and CP
       Dist = norm (Vec_IP2_CP-Vec3); % Distance between vectro IP1-IP2 to CP

       StrideWidth = Dist * Sign_side; %depending on body side

       % if the feet cross, calculate negative step width
       Vec_IP_3 = [Vec_IP,0];
       Vec_IP2_CP_3 = [Vec_IP2_CP,0];
       CrossProd = cross(Vec_IP_3,Vec_IP2_CP_3);
       if CrossProd(:,3) < 0 
          StrideWidth = StrideWidth * -1;
       end %IF cross(Vec_IP,Vec_IP2_CP) < 0 

       GaitParam.Dummy.Stride_Width.Left(i) = StrideWidth/1000; % transfer from millimeters to metres     

end %for-loop left events

end % function
