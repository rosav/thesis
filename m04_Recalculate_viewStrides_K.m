%% Recalculate_viewStrides_K
% as it is known that CV is influences by amount of strides, we evalute how
% our stats would have changes if we would have included a specific amount
% of strides.

% R. Visscher, May 2022

clc
%% Set-up
Group = 'CP';%'Orthosis';%CP, TD, Orthosis
n_steps=8;
n_runs=100;
SpatioTemporal_MDC = SpatioTemporal_CPTD;
list = fieldnames(SpatioTemporal_MDC.(Group));

%% Kinematics  - get Data
for i = 6:length(list)
    %% Extract data K
    % Calculate Mean, SD, CV
    
    [~, Strides_K_L] = size(SpatioTemporal_MDC.(Group).(list{i,1}).Angles_100.AnkleFlex.Left);
    [~, Strides_K_R] = size(SpatioTemporal_MDC.(Group).(list{i,1}).Angles_100.AnkleFlex.Right);
    
    for k=1:n_runs
        r_L= randi([1 Strides_K_L],1,n_steps);
        r_R = randi([1 Strides_K_R],1,n_steps);
        
        for j=1:length(r_L)
            n_L = r_L(j);
            K_L(:,j) = SpatioTemporal_MDC.(Group).(list{i,1}).Angles_100.AnkleFlex.Left(:,n_L);
            n_R = r_R(j);
            K_R(:,j) = SpatioTemporal_MDC.(Group).(list{i,1}).Angles_100.AnkleFlex.Right(:,n_R);
        end
        
        %meansd
        SpatioTemporal_steps.(Group).(list{i,1}).meanSD.AnkleFlex_L(k) = nanmean(nanstd(K_L'));
        SpatioTemporal_steps.(Group).(list{i,1}).meanSD.AnkleFlex_R(k) = nanmean(nanstd(K_R));
        SpatioTemporal_steps.(Group).(list{i,1}).meanSD.AnkleFlex_All(k) = (SpatioTemporal_steps.(Group).(list{i,1}).meanSD.AnkleFlex_L(k)+SpatioTemporal_steps.(Group).(list{i,1}).meanSD.AnkleFlex_L(k))/2;
    end%for n_runs
end %end for-loop subjects

%% Order data
% Order to large lists 

Subs = fieldnames(SpatioTemporal_steps.(Group));
for n_subs=1:length(Subs)
     
     T_CP_steps.eight_angles(n_subs,1:n_runs) = SpatioTemporal_steps.(Group).(Subs{n_subs,1}).meanSD.AnkleFlex_All;
    
end %end for-loop subjects
clear var Strides_ST_L Strides_ST_R r_L r_R ST_L ST_R SpatioTemporal_steps

%% Plot data
Overview.(Group).A_four = mean(T_CP_steps.four_angles);
Overview.(Group).A_six = mean(T_CP_steps.six_angles);
Overview.(Group).A_eight = mean(T_CP_steps.eight_angles);
Overview.(Group).A_all = mean(T_TDCP_recal.CP.AnkleFlex);

figure
hold on
histogram(Overview.(Group).six)
histogram(Overview.(Group).eight)
histogram(Overview.(Group).ten)
histogram(Overview.(Group).twelf)
plot(overview.(Group).all,25,'*r')


%% Set-up
Group = 'TD';%'Orthosis';%CP, TD, Orthosis
n_steps=8;
n_runs=100;
SpatioTemporal_MDC = SpatioTemporal_CPTD;
list = fieldnames(SpatioTemporal_MDC.(Group));

%% Kinematics  - get Data
for i = 35:42%1:12%length(list)
    %% Extract data K
    % Calculate Mean, SD, CV
    
    [~, Strides_K] = size(SpatioTemporal_MDC.(Group).(list{i,1}).Angles_100.AnkleFlex);

    
    for k=1:n_runs
        r= randi([1 Strides_K],1,n_steps);

        
        for j=1:length(r)
            n = r(j);
            K(:,j) = SpatioTemporal_MDC.(Group).(list{i,1}).Angles_100.AnkleFlex(:,n);

        end
        
        %meansd
        SpatioTemporal_steps.(Group).(list{i,1}).meanSD.AnkleFlex_All(k) = nanmean(nanstd(K'));
   end%for n_runs
end %end for-loop subjects

%% Order data
% Order to large lists 

Subs = fieldnames(SpatioTemporal_steps.(Group));
for n_subs=1:length(Subs)
     
     T_TD_steps.four_angles(n_subs,1:n_runs) = SpatioTemporal_steps.(Group).(Subs{n_subs,1}).meanSD.AnkleFlex_All;
    
end %end for-loop subjects
clear var Strides_ST_L Strides_ST_R r_L r_R ST_L ST_R SpatioTemporal_steps

%% Plot data
Overview.(Group).A_four = mean(T_TD_steps.four_angles);
Overview.(Group).A_six = mean(T_TD_steps.six_angles);
Overview.(Group).A_eight = mean(T_TD_steps.eight_angles);
Overview.(Group).A_all = mean(T_TDCP_recal.TD.AnkleFlex);

figure
hold on
histogram(Overview.(Group).six)
histogram(Overview.(Group).eight)
histogram(Overview.(Group).ten)
histogram(Overview.(Group).twelf)
plot(overview.(Group).all,25,'*r')
