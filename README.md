# Thesis

## Getting started
Here you will find the codes and data connected to: Visscher, R.M.S. (2022) "Assisting clinical decision making for paediatric movement disorders - towards assessing the effectiveness of therapy in cerebral palsy through mapping of gait maturation" ETH Doctoral Thesis.
